window.onload = () => {
  let selectedTags = [];
  let checkboxes = document.querySelectorAll('input[type="checkbox"]');
  checkboxes.forEach(function (checkbox, key, parent) {
    checkbox.value = false;
    checkbox.addEventListener("change", () => {
      let tag = checkbox.getAttribute("id");
      let tagIsSelected = selectedTags.includes(tag);
      if (tagIsSelected) {
        let tagIndex = selectedTags.indexOf(tag);
        selectedTags.splice(tagIndex, 1);
      } else {
        selectedTags.push(tag);
      }
      // Show only the links that contain the selected tags.
      let links = document.querySelectorAll("li[data-tags]");
      links.forEach(function (link, key, parent) {
        var linkTags = link.getAttribute("data-tags").split(",");
        let result = selectedTags.every(function (tag) {
          return linkTags.includes(tag);
        });
        link.style.display = result ? "list-item" : "none";
      });
    });
  });
};
