// https://www.cginternals.com/en/blog/2018-01-10-screen-aligned-quads-and-triangles.html
const screenAlignedTriangle = [
  [-1, -1],
  [3, -1],
  [-1, 3],
];
