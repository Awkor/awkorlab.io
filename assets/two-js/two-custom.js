class VectorShape extends Two.Group {
  static DEFAULT_TIP_RADIUS = 5.5;

  constructor(parameters) {
    super();

    // TODO: Make color changeable at runtime.
    this.color = parameters.color;
    this.position = parameters.position ?? this.position;
    this.tipRadius = parameters.tipRadius ?? VectorShape.DEFAULT_TIP_RADIUS;
    this.value = parameters.value;

    if (parameters.drawCircle) {
      this.circle = new Two.Circle(0, 0, this.tipRadius / 2);
      this.circle.fill = this.color;
      this.circle.stroke = "transparent";
      this.add(this.circle);
    }

    this.line = new Two.Line();
    this.line.stroke = this.color;
    this.add(this.line);

    // TODO: Scale the tip down if the vector is smaller than its radius.
    this.triangle = new Two.Polygon(0, 0, this.tipRadius, 3);
    this.triangle.fill = this.color;
    this.triangle.stroke = "transparent";
    this.add(this.triangle);
  }

  _update() {
    // TODO: Comment what we are doing here.
    const length = this.value.length();
    this.triangle.radius = Math.min(length / 2, this.tipRadius);
    const scale = (length - this.triangle.radius) / length;
    const trianglePosition = this.value.clone().multiply(scale);
    this.line.vertices[1].x = trianglePosition.x;
    this.line.vertices[1].y = trianglePosition.y;

    const angle = Math.atan2(this.value.y, this.value.x) + Two.Utils.HALF_PI;
    this.triangle.position = trianglePosition;
    this.triangle.rotation = angle;

    return super._update.apply(this, arguments);
  }
}

Two.prototype.makeVector = function (parameters) {
  const vector = new VectorShape(parameters);
  this.scene.add(vector);

  return vector;
};
