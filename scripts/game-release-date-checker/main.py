import math
import sys
import time
import urllib.request
from enum import Enum, auto

import tomli
from bs4 import BeautifulSoup
from dateutil import parser as date_parser
from rich.console import Console
from rich.progress import BarColumn, MofNCompleteColumn, Progress

console = Console()
pause_duration = math.pi
progress = Progress(BarColumn(), MofNCompleteColumn())

gog_date_selector = (
    "div.details__rating:nth-child(4) > div:nth-child(2) > span:nth-child(1)"
)
steam_date_selector = ".date"


class Website(Enum):
    GOG = auto()
    STEAM = auto()


def get_date(url, website):
    page = urllib.request.urlopen(url)
    soup = BeautifulSoup(page, "html.parser")

    selector = None
    match website:
        case Website.GOG:
            selector = gog_date_selector
        case website.STEAM:
            selector = steam_date_selector
    selection = soup.select(selector)

    if selection:
        date_string = None
        match website:
            case Website.GOG:
                date_string = selection[0].contents[0].split("'")[1]
            case website.STEAM:
                date_string = selection[0].contents[0]
        return date_parser.parse(date_string)


def main():
    file_path = sys.argv[1]
    toml_file = open(file_path, "r")
    toml_lines = toml_file.read()
    games = tomli.loads(toml_lines).items()

    with progress:
        for game_name, values in progress.track(games):
            if "year" not in values:
                continue
            year = values["year"]
            link = values["link"]

            website = None
            if "gog.com" in link:
                website = Website.GOG
            if "steampowered.com" in link:
                website = Website.STEAM

            if website is None:
                message = f"{game_name}: [yellow]Skipped[/yellow]"
                progress.console.print(message)
                continue

            website_date = get_date(link, website)
            result = (
                "[green]Correct[/green]"
                if year == website_date.year
                else "[red]Wrong[/red]"
            )
            message = f"{game_name}: {result} (TOML: {year}) ({website.name}: {website_date.year})"
            progress.console.print(message)

            time.sleep(pause_duration)


if __name__ == "__main__":
    main()
