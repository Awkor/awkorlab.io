const Vector2 = THREE.Vector2
const Vector3 = THREE.Vector3

const HOLE_DISTANCE = new Vector2(38.3, 30.86);
const HOLE_DIAMETER = 3
const HOLE_MARGIN = 2.565

const BASE_SIZE = new Vector3(
    HOLE_DISTANCE.x * 2 + HOLE_DIAMETER + HOLE_MARGIN * 2,
    HOLE_DISTANCE.y * 2 + HOLE_DIAMETER + HOLE_MARGIN * 2,
    3
)
const BASE_CHAFER_DISTANCE = 3
const BASE_HOLE_SIZE = new Vector3(
    BASE_SIZE.x - HOLE_DIAMETER * 2 - HOLE_MARGIN * 4,
    BASE_SIZE.y - HOLE_DIAMETER * 2 - HOLE_MARGIN * 4,
    BASE_SIZE.z
)

const PLATFORM_HOLE_DISTANCE = new Vector2(29, 24.5);
const PLATFORM_HOLE_OFFSET = HOLE_DISTANCE.x - PLATFORM_HOLE_DISTANCE.x

const hole = Cylinder(HOLE_DIAMETER / 2, BASE_SIZE.z, true)
const holes = [
    Translate([-HOLE_DISTANCE.x, +HOLE_DISTANCE.y, 0], hole),
    Translate([+HOLE_DISTANCE.x, +HOLE_DISTANCE.y, 0], hole),
    Translate([-HOLE_DISTANCE.x, -HOLE_DISTANCE.y, 0], hole),
    Translate([+HOLE_DISTANCE.x, -HOLE_DISTANCE.y, 0], hole),
]

function Hexagon(width, depth) {
  const degreesToRadians = Math.PI / 180
  const height = width / Math.tan(degreesToRadians * 60);
  const box = Box(width, height, depth, true);
  const rotationAxis = [0, 0, 1];
  return Union(
    [box, Rotate(rotationAxis, 60, box), Rotate(rotationAxis, 120, box)],
    false
  );
}

function HexagonalSocket(width, depth, margin) {
    const hole = Hexagon(width, depth)
    const socket = Hexagon(width + margin, depth)
    return Difference(socket, [hole])
}

const HEX_HEIGHT = 2.4
const hex = HexagonalSocket(2.5, HEX_HEIGHT, HOLE_MARGIN)
let platform_holes = [
    Translate([-PLATFORM_HOLE_DISTANCE.x + PLATFORM_HOLE_OFFSET, +PLATFORM_HOLE_DISTANCE.y, BASE_SIZE.z - (BASE_SIZE.z - HEX_HEIGHT)], hex),
    Translate([+PLATFORM_HOLE_DISTANCE.x + PLATFORM_HOLE_OFFSET, +PLATFORM_HOLE_DISTANCE.y, BASE_SIZE.z - (BASE_SIZE.z - HEX_HEIGHT)], hex),
    Translate([-PLATFORM_HOLE_DISTANCE.x + PLATFORM_HOLE_OFFSET, -PLATFORM_HOLE_DISTANCE.y, BASE_SIZE.z - (BASE_SIZE.z - HEX_HEIGHT)], hex),
    Translate([+PLATFORM_HOLE_DISTANCE.x + PLATFORM_HOLE_OFFSET, -PLATFORM_HOLE_DISTANCE.y, BASE_SIZE.z - (BASE_SIZE.z - HEX_HEIGHT)], hex),
]

let base = Box(BASE_SIZE.x, BASE_SIZE.y, BASE_SIZE.z, true)
base = ChamferEdges(base, BASE_CHAFER_DISTANCE, [0, 2, 4, 6])
let base_hole = Box(BASE_HOLE_SIZE.x, BASE_HOLE_SIZE.y, BASE_HOLE_SIZE.z, true)
base = Difference(base, [base_hole])
base = ChamferEdges(base, BASE_CHAFER_DISTANCE, [32, 33, 34, 35])
let support = Box(HOLE_DIAMETER + HOLE_MARGIN * 2, BASE_SIZE.y, BASE_SIZE.z, true)
support = Translate([-PLATFORM_HOLE_DISTANCE.x + PLATFORM_HOLE_OFFSET, 0, 0], support)
base = Union([base, support])
base = ChamferEdges(base, BASE_CHAFER_DISTANCE, [23, 26, 25, 28])
base = Difference(base, holes)

platform_holes.push(base)

base = Union(platform_holes)
