const Vector2 = THREE.Vector2
const Vector3 = THREE.Vector3

const HOLE_DIAMETER = 3.1
const HOLE_DEPTH = 3
const HOLE_MARGIN = 3

const HARD_DISK_GAP = 3
const FLOOR_GAP = 3

const HARD_DISK_SMALL_SIZE = new Vector3(100.2, 69.85, 6.8)
const HARD_DISK_BIG_SIZE = new Vector3(100.4, 69.85, 9.5)
const HARD_DISK_HOLE_SPACING = 76.6;

const HOLE_X = HARD_DISK_HOLE_SPACING / 2;
const HOLE_Y = (HARD_DISK_GAP + HARD_DISK_SMALL_SIZE.z) / 2;

const SIDE_EXTENSION = 3
const SIDE_SIZE = new Vector3(
    100.4,
    SIDE_EXTENSION + HARD_DISK_BIG_SIZE.z + FLOOR_GAP,
    3
)
const SIDE_HOLE_SIZE = new Vector3(
    SIDE_SIZE.x - HOLE_DIAMETER * 2 - HOLE_MARGIN * 2,
    SIDE_SIZE.y - HOLE_DIAMETER * 2 - HOLE_MARGIN * 2,
    SIDE_SIZE.z
)

const SIDE_COLUMN_SIZE = new Vector3(
    HOLE_DIAMETER + HOLE_MARGIN,
    SIDE_SIZE.y,
    SIDE_SIZE.z
)

function HardDisks() {
    let hard_disk_big = Box(HARD_DISK_BIG_SIZE.x, HARD_DISK_BIG_SIZE.y, HARD_DISK_BIG_SIZE.z)
    // let hard_disk_small = Box(HARD_DISK_SMALL_SIZE.x, HARD_DISK_SMALL_SIZE.y, HARD_DISK_SMALL_SIZE.z)
    {
        let hole = Cylinder(HOLE_DIAMETER / 2, 3, true)
        hole = Rotate([1, 0, 0], 90, hole)
        let holes = [
            Translate([14, HOLE_DEPTH / 2, HOLE_MARGIN], hole),
            Translate([90.6, HOLE_DEPTH / 2, HOLE_MARGIN], hole),
            Translate([14, HARD_DISK_BIG_SIZE.y - HOLE_DEPTH / 2, HOLE_MARGIN], hole),
            Translate([90.6, HARD_DISK_BIG_SIZE.y - HOLE_DEPTH / 2, HOLE_MARGIN], hole)
        ]
        hard_disk_big = Difference(hard_disk_big, holes)
        // hard_disk_small = Difference(hard_disk_small, holes)
    }

    hard_disk_big = Rotate([0, 1, 0], 180, hard_disk_big)
    // hard_disk_small = Rotate([0, 1, 0], 180, hard_disk_small)

    hard_disk_big = Translate([HARD_DISK_BIG_SIZE.x / 2, 0 - HARD_DISK_BIG_SIZE.y / 2, HARD_DISK_BIG_SIZE.z], hard_disk_big)
    // hard_disk_small = Translate([HARD_DISK_SMALL_SIZE.x / 2, 0 - HARD_DISK_SMALL_SIZE.y / 2, HARD_DISK_SMALL_SIZE.z], hard_disk_small)

    hard_disk_big = Translate([0, 0, FLOOR_GAP], hard_disk_big)
    // hard_disk_small = Translate([0, 0, FLOOR_GAP + HARD_DISK_BIG_SIZE.z + HARD_DISK_GAP], hard_disk_small)
}

// HardDisks()

let hole = Cylinder(HOLE_DIAMETER / 2, HOLE_DEPTH, true)
let holes = Union([
    Translate([-HOLE_X, +HOLE_Y, 0], hole),
    Translate([+HOLE_X, +HOLE_Y, 0], hole),
    // Translate([-HOLE_X, -HOLE_Y, 0], hole),
    // Translate([+HOLE_X, -HOLE_Y, 0], hole),
])
holes = Translate([0, SIDE_SIZE.y / 2 - SIDE_EXTENSION - HOLE_MARGIN - HOLE_Y, 0], holes)

let side = Box(SIDE_SIZE.x, SIDE_SIZE.y, SIDE_SIZE.z, true)

// let side = Box(SIDE_SIZE.x, SIDE_SIZE.y, SIDE_SIZE.z, true)
side = Difference(side, [holes])
// let side_hole = Box(SIDE_HOLE_SIZE.x, SIDE_HOLE_SIZE.y, SIDE_HOLE_SIZE.z, true)
// side = Difference(side, [side_hole])
// let side_column = Box(SIDE_COLUMN_SIZE.x, SIDE_COLUMN_SIZE.y, SIDE_COLUMN_SIZE.z, true)
// side = Union([side, side_column])
// side = ChamferEdges(side, 3, [0, 2, 6, 21, 36, 37, 38, 39, 42, 43, 44, 45])

side = ChamferEdges(side, 3, [0, 2, 5, 12])

side = Rotate([1, 0, 0], 90, side)
side = Translate([0, 0 - HARD_DISK_BIG_SIZE.y / 2 - HOLE_DEPTH / 2, SIDE_SIZE.y / 2], side)

// let side_bottom = Box(SIDE_WIDTH, SIDE_BOTTOM_HEIGHT, HOLE_DEPTH, true)
// side_bottom = Translate([0, 0 - SIDE_HEIGHT / 2 - SIDE_BOTTOM_HEIGHT / 2 + 3, 0], side_bottom)

// let side_hole = Box(SIDE_HOLE_SIZE.x, SIDE_HOLE_SIZE.y, SIDE_HOLE_SIZE.z, true)
// side_hole = Translate([0, 0, 0], side_hole)
