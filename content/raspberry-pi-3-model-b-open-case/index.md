---
title: Raspberry Pi 3 Model B Open Case
---

This is an open case for a Raspberry Pi 3 Model B.

It's meant to be mounted on top of an SSD or 2.5" HDD.

All of these models were made with [CascadeStudio](https://github.com/zalo/CascadeStudio).

{{< cascade-studio fileName="top" >}}

{{< cascade-studio fileName="base" >}}

{{< cascade-studio fileName="side" >}}
