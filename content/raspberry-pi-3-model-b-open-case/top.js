const Vector2 = THREE.Vector2;
const Vector3 = THREE.Vector3;

const BOARD_CORNER_RADIUS = 3;
const BOARD_FILLET_RADIUS = 2;
const BOARD_HOLE_DISTANCE = new Vector2(58, 49);
const BOARD_HOLE_MARGIN = 3.5;
const BOARD_HOLE_RADIUS = 1.35;
const BOARD_SIZE = new Vector3(85, 56, 1.6);
const BOARD_HOLE_OFFSET =
  (BOARD_SIZE.x - BOARD_HOLE_DISTANCE.x) / 2 - BOARD_HOLE_MARGIN;

const COLUMN_CHAMFER_DISTANCE = 2;
const COLUMN_DIAMETER = BOARD_HOLE_RADIUS * 2 + BOARD_HOLE_MARGIN;
const COLUMN_HOLE_RADIUS = BOARD_HOLE_RADIUS;
const COLUMN_SIZE = new Vector3(COLUMN_DIAMETER, COLUMN_DIAMETER, 13.5);

const FAN_FILLET_RADIUS = 2;
const FAN_HOLE_RADIUS = 19;
const FAN_SCREW_HOLE_RADIUS = 2;
const FAN_SCREW_HOLE_SPACING = 32;
const FAN_SIZE = new Vector3(40, 40, 10);

const PLATFORM_CHAMFER_DISTANCE = 2;
const PLATFORM_SIZE = new Vector3(
  BOARD_HOLE_DISTANCE.x + COLUMN_SIZE.x,
  BOARD_HOLE_DISTANCE.y + COLUMN_SIZE.y,
  3
);

const COLUMN_X = BOARD_HOLE_DISTANCE.x / 2;
const COLUMN_Y = BOARD_HOLE_DISTANCE.y / 2;
const COLUMN_Z = (PLATFORM_SIZE.z - COLUMN_SIZE.z) / 2;

function Combination(x, y, z, shape) {
  return [
    Translate([-x, +y, z], shape),
    Translate([+x, +y, z], shape),
    Translate([-x, -y, z], shape),
    Translate([+x, -y, z], shape),
  ];
}

function Board() {
  let shape = Box(BOARD_SIZE.x, BOARD_SIZE.y, BOARD_SIZE.z, true);
  {
    const edgeList = [0, 2, 4, 6];
    shape = FilletEdges(shape, BOARD_CORNER_RADIUS, edgeList);
  }
  {
    const x = BOARD_HOLE_DISTANCE.x / 2;
    const y = BOARD_HOLE_DISTANCE.y / 2;
    const offset =
      (BOARD_SIZE.x - BOARD_HOLE_DISTANCE.x) / 2 - BOARD_HOLE_MARGIN;
    const hole = Cylinder(BOARD_HOLE_RADIUS, BOARD_SIZE.z, true);
    const holes = [
      Translate([-x - offset, +y, 0], hole),
      Translate([+x - offset, +y, 0], hole),
      Translate([-x - offset, -y, 0], hole),
      Translate([+x - offset, -y, 0], hole),
    ];

    shape = Difference(shape, holes);
  }
  return shape;
}

function Fan() {
  let shape = Box(FAN_SIZE.x, FAN_SIZE.y, FAN_SIZE.z, true);
  {
    const edgeList = [0, 2, 4, 6];
    shape = FilletEdges(shape, FAN_FILLET_RADIUS, edgeList);
  }
  {
    const hole = Cylinder(FAN_HOLE_RADIUS, FAN_SIZE.z, true);
    shape = Difference(shape, [hole]);
  }
  {
    const halfHoleSpacing = FAN_SCREW_HOLE_SPACING / 2;
    const hole = Cylinder(FAN_SCREW_HOLE_RADIUS, FAN_SIZE.z, true);
    const holes = Combination(halfHoleSpacing, halfHoleSpacing, 0, hole);
    shape = Difference(shape, holes);
  }
  return shape;
}

function Column() {
  let shape = Box(COLUMN_SIZE.x, COLUMN_SIZE.y, COLUMN_SIZE.z, true);
  {
    const chamferEdges = [0, 2, 4, 6];
    shape = ChamferEdges(shape, COLUMN_CHAMFER_DISTANCE, chamferEdges);
  }
  {
    const hole = Cylinder(COLUMN_HOLE_RADIUS, COLUMN_SIZE.z, true);
    shape = Difference(shape, [hole]);
  }
  return shape;
}

function Platform() {
  let shape = Box(PLATFORM_SIZE.x, PLATFORM_SIZE.y, PLATFORM_SIZE.z, true);
  {
    const width = PLATFORM_SIZE.x - COLUMN_SIZE.x * 2;
    const height = (PLATFORM_SIZE.y - FAN_SIZE.y) / 2;
    const y = (height - PLATFORM_SIZE.y) / 2 - 1.5;
    let cut = Box(width, height, PLATFORM_SIZE.z, true);
    cut = Translate([0, -y, 0], cut);
    shape = Difference(shape, [cut]);
  }
  {
    const edgeList = [0, 2, 5, 14, 20, 21, 22, 23];
    shape = ChamferEdges(shape, PLATFORM_CHAMFER_DISTANCE, edgeList);
  }
  {
    const hole = Cylinder(FAN_HOLE_RADIUS, PLATFORM_SIZE.z, true);
    shape = Difference(shape, [hole]);
  }
  {
    const hole = Cylinder(FAN_SCREW_HOLE_RADIUS, PLATFORM_SIZE.z, true);
    const x = FAN_SCREW_HOLE_SPACING / 2;
    const y = FAN_SCREW_HOLE_SPACING / 2;
    const holes = Combination(x, y, 0, hole);
    shape = Difference(shape, holes);
  }
  {
    const hole = Cylinder(BOARD_HOLE_RADIUS, PLATFORM_SIZE.z, true);
    const holes = Combination(COLUMN_X, COLUMN_Y, 0, hole);
    shape = Difference(shape, holes);
  }
  return shape;
}

const board = Board();

const column = Column();
const columns = Combination(COLUMN_X, COLUMN_Y, COLUMN_Z, column);

let fan = Fan();
fan = Translate(
  [-BOARD_HOLE_OFFSET, 0, (BOARD_SIZE.z + FAN_SIZE.z) / 2 + COLUMN_SIZE.z],
  fan
);

let platform = Platform();

platform = Union([...columns, platform], false);
platform = Translate(
  [-BOARD_HOLE_OFFSET, 0, (BOARD_SIZE.z - PLATFORM_SIZE.z) / 2 + COLUMN_SIZE.z],
  platform
);
