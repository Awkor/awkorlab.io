---
title: Dot Product
---

```rust
a.dot(b) = a.x * b.x + a.y * b.y
```

{{< two-js fileName="dot-product.js" >}}

Thanks to [Freya Holmér](https://nitter.net/FreyaHolmer) for her pretty [visualization](https://nitter.net/FreyaHolmer/status/1200807790580768768).