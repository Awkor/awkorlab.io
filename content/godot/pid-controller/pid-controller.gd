class_name PIDController
extends Reference

var proportional_weight := 1.0
var integral_weight := 1.0
var derivative_weight := 1.0

var _integral := 0.0
var _previous_error := 0.0


func update(error: float, delta: float) -> float:
	var proportional := error
	var weighted_proportional := proportional * proportional_weight

	_integral += error * delta
	var weighted_integral := _integral * integral_weight

	var derivative := (error - _previous_error) / delta
	var weighted_derivative := derivative * derivative_weight

	_previous_error = error
	return weighted_proportional + weighted_integral + weighted_derivative
