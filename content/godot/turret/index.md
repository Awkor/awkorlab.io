---
title: Turret
---

{{< source-code fileName="turret.gd" language="gd" >}}

From here, you can change things to better fit your needs. For example, you could make the rotation limits non-symmetric (e.g. the turret could aim higher than lower), but that's left as an exercise for the reader :)

Thanks to [Why485](https://nitter.net/Why485) for making [GunTurrets2](https://github.com/brihernandez/GunTurrets2).
