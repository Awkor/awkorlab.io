---
title: Screen Transition Textures
---

{{< shader fragmentFileName="swipe.fragment" vertexFileName="common.vertex" >}}

{{< shader fragmentFileName="split.fragment" vertexFileName="common.vertex" >}}

{{< shader fragmentFileName="radial.fragment" vertexFileName="common.vertex" >}}

{{< shader fragmentFileName="stripes.fragment" vertexFileName="common.vertex" >}}

[SHADERed](https://github.com/dfranx/SHADERed) can be used to render the output of these shaders to an image.

[Shaders Case Study - Pokémon Battle Transitions](https://www.youtube.com/watch?v=LnAoD7hgDxw) by [Makin' Stuff Look Good](https://www.youtube.com/channel/UCEklP9iLcpExB8vp_fWQseg) is worth watching if you want to see more about transitions and shaders.

[WebGL 1.0 API Quick Reference Card](https://www.khronos.org/files/webgl/webgl-reference-card-1_0.pdf)
